=== Guessing a Number

In this section let's see method to guess a number with hot and cold learning. So fire up your Jupyter lab, we first assign a variable called `number` to 42 as shown:

```julia
number = 42
```

Output


    42

Now let's pick a random guess from 1 to hundred

```julia
guess = rand(0:100)
```

Output


    21

Let's define our learning rate alpha as 0.1


```julia
α = 0.1
```

Output


    0.1


Now we compute the error between our target `number` and `guess`


```julia
error = abs(number - guess)
```


Output

    21


Our `error` is 21 as shown above, now we make a guess called hot guess where we add `α` to `guess`

```julia
# hot
hot_guess = guess + α
```

Output


    21.1

Similarly we make a `cold_guess` where we subtract α` from `guess`


```julia
# cold
cold_guess = guess - α
```

Output


    20.9

Now we compute the hot error which is absolute distance between `hot_guess` and `number` and call it as `hot_error`


```julia
hot_error = abs(number - hot_guess)
```

Output


    20.9

Similarly we also compute `cold_error`


```julia
cold_error = abs(number - cold_guess)
```

Output


    21.1

Now the `hot_error` is lesser than `cold_error`, so we update our new `guess` value to `hot_guess` thus reducing the error.

Now let's do it many times:


```julia
for i in 1:1000
    error = abs(number - guess)
    hot_guess = guess + α
    hot_error = abs(number - hot_guess)
    cold_guess = guess - α
    cold_error = abs(number - cold_guess)

    if hot_error < error
        guess = hot_guess
    elseif cold_error < error
        guess = cold_guess
    end
end

guess
```

Output


    42.0000000000003

By updating `guess` to either `hot_guess` or `cold_guess` depending on which error is smaller as we see in the code snippet shown below:

```julia
if hot_error < error
    guess = hot_guess
elseif cold_error < error
    guess = cold_guess
end
```
we nudge `guess` closer to `number`. Finally we get our guess as 42.0000000000003, which is almost exact as `number`.

==== Plotting what we had done

Now let's plot what we have done to get understanding. Over here we have set `number` to 5 and are guessing initially for any number to be from 1 to 10., then we iterate 100 time.

```julia
number = 5
guess = rand(0:10)
α = 0.1
guesses = []

for i in 1:100
    error = abs(number - guess)
    hot_guess = guess + α
    hot_error = abs(number - hot_guess)
    cold_guess = guess - α
    cold_error = abs(number - cold_guess)

    if hot_error < error
        guess = hot_guess
    elseif cold_error < error
        guess = cold_guess
    end
    push!(guesses, guess)
end

guess
```

Output


    4.999999999999998

If you see in the code above we record our guesses in an Array named `guesses`, now lets try to plot them:

First let's import our `Plots` package

```julia
using Plots
```


```julia
number_to_be_guessed = [number for i in 1:100]

guess_plot = plot!(1: 100, number_to_be_guessed,
    label = "number to guess", title = "Hot & Cold - Guesssing Number" )

scatter!(guess_plot, 1:100, guesses, label = "guesses")
```

Output

image::plots/hot_and_cold_guessing_number/output_14_0.svg[]

In the code above we have a variable called `number_to_be_guessed`, which is nothing number array of 100 `number`.

We first create a `guess_plot` that just creates a straight line which represents the number to be guessed using this piece of code:

```julia
guess_plot = plot!(1: 100, number_to_be_guessed,
    label = "number to guess", title = "Hot & Cold - Guesssing Number" )
```

Now we add the `guesses` we recorded in the previous `for` loop to the `guess_plot` as shown:

```julia
scatter!(guess_plot, 1:100, guesses, label = "guesses")
```

If you see in the plot above looks like the `guess` was initially 2, and slowly it raises above to 5.
