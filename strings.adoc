== Strings

NOTE: Video lecture for this section could be found here https://youtu.be/FXNIuZuUPLs

NOTE: Get the Jupyter notebook of this blog here https://gitlab.com/datascience-book/code/-/blob/master/strings.ipynb

Imagine a perl necklace, you have a string that runs through pearls, that is you have stringed or strung the pearls. If you string bunch of characters in programming, it's called a `String`. as simple as that.

So now let's create a  Hello World string as shown below:


```julia
"Hello World!"
```

Output:


    "Hello World!"

There is a lot to learn here. We find characters `H` to `!` surrounded by double quotes `"`, they were strung. That how you create a string.

Now let's print it out using a function named `println` (I think this means print line)


```julia
println("Hello World!")
```

Output:

    Hello World!


Now let's put a `String` into a variable named `string`


```julia
string =  "I am a string"
```

Output:


    "I am a string"


And let's print out the variable `string`


```julia
println(string)
```

Output:

    I am a string

Look when you print it, how neat it is without those ugly double quotes.

Now let's assign `42` to a variable named `a`


```julia
a = 42
```

Output:


    42

Check its type. It would be `Int` anyway


```julia
typeof(a)
```

Output:


    Int64


Now let's use a function named `repr` to convert it into `String`.


```julia
b = repr(a)
```

Output:


    "42"

I am not sure what's the meaning or full form of `repr`, but thats a weird function name in Julia.

Take a look at the code below, execute it in your notebook.


```julia
c = "Answer to the ultimate question $a"
```

Output:

    "Answer to the ultimate question 42"

We see that `"Answer to the ultimate question $a"`, where `$a` tells Julia to embed the value of `a` in the string. Technically this is called *String Interopolation*.

Now let's print `c` and see


```julia
println(c)
```

Output:

    Answer to the ultimate question 42

There is a function called `occursin` that checks is a string occurs in another string. To test it, let's create a string and store it in a variable called sentence as shown below:

```julia
sentence = "A spammer is a person who eats spam"
```


Output:

    "A spammer is a person who eats spam"


Now let's check if `"spam"` occurs in `sentence`


```julia
occursin("spam", sentence)
```

Output:


    true

and it does!

If you want to find in which location `"spam"` occurs first, you can use function called `findfirst` as shown below:

```julia
findfirst("spam", sentence)
```

Ouput:


    3:6

It gives the range, that is `"spam"` can be found from 3rd character to 6th character of `sentence`.

Now if you want to find all occurance of `"spam"` in `sentence`, you can use `findall` as shown below:


```julia
findall("spam", sentence)
```

Output:


    2-element Vector{UnitRange{Int64}}:
     3:6
     32:35

Now let's check if 32nd to 35th character of senctence is really `"spam"`


```julia
sentence[32:35]
```

Output:


    "spam"


If you have not guessed it, `String` can be thought of as a `Array` of characters, to kinda prove it, we check  the 7th location of `sentence`

```julia
sentence[7]
```

Output:


    'm': ASCII/Unicode U+006D (category Ll: Letter, lowercase)

and its `'m'`.

Now let's check what are the first seven characters of `sentence`


```julia
sentence[1:7]
```

Output:


    "A spamm"

No let's check what we have from 7th character to the last of `sentence`, here to get the last place we use `length(sentence)`:


```julia
sentence[7:length(sentence)]
```

Output:


    "mer is a person who eats spam"


Like we can iterate element by element in an `Array`, we can iterate character by character in a `String` and print them out as shown:

```julia
for character in sentence
    print("$character,")
end
```

Output:

    A, ,s,p,a,m,m,e,r, ,i,s, ,a, ,p,e,r,s,o,n, ,w,h,o, ,e,a,t,s, ,s,p,a,m,

Just like we can use `join` function on `Array`, we can use them on `String` as shown:

```julia
join(sentence, ",")
```


Output:

    "A, ,s,p,a,m,m,e,r, ,i,s, ,a, ,p,e,r,s,o,n, ,w,h,o, ,e,a,t,s, ,s,p,a,m"


This section doesn't go into regular expressions, but we can find if a regular expression is found in a `String` using the `match` function as shown:

```julia
m = match(r"spam", sentence)
```


Output:

    RegexMatch("spam")

We might deep dive into regular expressions possibly when we are learning about Natural Language Processing.


We can use the function `uppercase` to convert all characters in a sentence to uppercase as shown:

```julia
uppercase(sentence)
```

Output:


    "A SPAMMER IS A PERSON WHO EATS SPAM"


Similarly, there is a way to convert it to lowercase too:

```julia
lowercase(c)
```

Output:


    "answer to the ultimate question 42"

This section is a brief introduction to `String` in Julia. I think this is enough of Strings for a Datascientist.


