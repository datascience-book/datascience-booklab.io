= Mathematics

include::vectors.adoc[]

include::matrices.adoc[]

include::sigmoid.adoc[]

== Bayesian

include::statistics.adoc[]

include::probability.adoc[]
