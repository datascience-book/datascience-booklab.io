== Hot and Cold Learning

NOTE: Video lecture for this section could be found here https://youtu.be/QAbYN2-AJIo

Imagine you are in  a room, the room has a facility to heat and cool itself. You find the room to be hot and so you lower the thermostat, then the room chills down and it appears that you have kept the thermostat too low, now  so  you raise the thermostat again so that the room becomes little warmer. Sooner or later with little adjustments you get the right temperature.

Now imagine a learning system where you either make some parameters higher or lower so that optimum parameters are reached for learning, that form of learning is called hot and cold learning.

include::hot_and_cold_guessing_number.adoc[]

include::hot_and_cold_fitting_a_line.adoc[]
