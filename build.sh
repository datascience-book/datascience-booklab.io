asciidoctor -a allow-uri-read -o ../dsbuk_output/datascience-book.html book.adoc
asciidoctor-pdf -a allow-uri-read -r asciidoctor-mathematical -o ../dsbuk_output/datascience-book.pdf book.adoc
asciidoctor-epub3 -a allow-uri-read book.adoc -o ../dsbuk_output/datascience-book.epub
