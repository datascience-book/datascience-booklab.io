== Regular Expressions (regexp)

NOTE: Video lecture for this section could be found here https://youtu.be/AkYyjhEizFA

NOTE: Get the Jupyter notebook here https://gitlab.com/datascience-book/code/-/blob/master/regexp.ipynb

Regular expressions are used to check if a pattern exists in a string. Without much blah blah let's see what it is.

=== A taste of Regexp

Type the code below and execute

[source, julia]
----
regexp = r"abc"
m = match(regexp, "english letters start with abc")
----

Output:

----
RegexMatch("abc")
----

Take this statement:

```julia
regexp = r"abc"
```

Here you have a variable named `regexp` which is been assigned to `r"abc"`, which is not a string. Even thought  the `abc` are surrounded by double quotes, the `r` before the quotes makes it an expression.

Now in this line:

```julia
m = match(regexp, "english letters start with abc")
```

Julia see's if the expression (not string) `abc` is present in the string `"english letters start with abc"`, and it is present it returns a match, and hence we get the output `RegexMatch("abc")`.


In the example below, you see that circular brackets surround `abc` and make it `(abc)`. This means we want to capture that expression, let's executeand see what happens:

[source, julia]
----
regexp = r"(abc)"
m = match(regexp, "english letters start with abc")
----

Output:

----
RegexMatch("abc", 1="abc")
----

So we get the output that it matches `abc`, which is denoted by `RegexMatch("abc"`, and this is followed by capture which is denoted by `1="abc")`.

Now you can get this match is captured in variable `m` and you can get it as shown below:


[source, julia]
----
m[1]
----


Output:

----
"abc"
----

These things are called captures.

`\d` is used to match a digit. In the example below string `"Five means 5 in English."` contains digit `5` and so `match` returns a positive match:


[source, julia]
----
regexp = r"\d"
m = match(regexp, "Five means 5 in English.")
----


Output:

----
RegexMatch("5")
----

Now let's say we wants to match exactly 6 digits. We know `\d` means match a digit, and we append `{6}` to it to match exactly 6 times as shown below:


[source, julia]
----
regexp = r"\d{6}"
m = match(regexp, "I live in 36/1, my postal code is 600131.")
----

Output:

----
RegexMatch("600131")
----

In the above example `36` consists of digits and so does `1` in `36/1`, they don't match `\d{6}` because they are two and one digit each, the one that matches is `600131`.


The plus sign denotes one or more so `\d+` means we need to match one or more digit. So the below example matches `36` in `"I live in 36/1, my postal code is 600131."`:

[source, julia]
----
regexp = r"(\d+)"
m = match(regexp, "I live in 36/1, my postal code is 600131.")
----

Output:

----
RegexMatch("36", 1="36")
----

since we have given it like `(\d+)` which means capture, we get the output as `RegexMatch("36", 1="36")`.

The example below shows how to check for a match:


[source, julia]
----
regexp = r"(\d{6})"
m = match(regexp, "I live in 36/1, my postal code is 600131.")

if m != nothing
    println("There is a match")
end
----


Output:

----
There is a match
----

`600131` is a match for `(\d{6})`, so there is a match. You can check if there is a match by capturing it in  a variable, in this case its `m`, and if there is a match it should not be `nothing`. So any thing inside this `if` block:and

```julia
if m != nothing
    println("There is a match")
end
```

Get's executed if there is a match, so the code prints `There is a match`.


In the code below we check for a 6 digit string which is not present in `"I live in 36/1, my postal code is 600."`, and hence the condition `m != nothing` becomes `false` and so nothing gets printed.


[source, julia]
----
regexp = r"(\d{6})"
m = match(regexp, "I live in 36/1, my postal code is 600.")

if m != nothing
    println("There is a match")
end
----

The code below is same as the example above, but here we have got the `else` part, since `m != nothing` becomes `false`, the else part gets executed and `Match not found` gets printed.


[source, julia]
----
regexp = r"(\d{6})"
m = match(regexp, "I live in 36/1, my postal code is 600.")

if m != nothing
    println("There is a match")

    else

    println("Match not found")
end
----


Output:
----
Match not found
----

Hope you have got some vague idea about regexp, we will see it in bit more detail in the coming sections.

=== Things to remember

There are some things you need to remember, or at least refer from time to time. Those are mentioned in table belowfootnote:[I got this list from http://rubular.com/].

If you do not understand now, don't worry, you will be able to pick it up.

|===
| Thing | What it means

| .
| Any single character

| \w
| Any word character (letter, number, underscore)

| \W
| Any non-word character

| \d
| Any digit

| \D
| Any non-digit

| \s
| Any whitespace character

| \S
| Any non-whitespace character

| \b
| Any word boundary character

| ^
| Start of line

| $
| End of line

| \A
| Start of string

| \z
| End of string

| [abc]
| A single character of: a, b or c

| [^abc]
| Any single character except: a, b, or c

| [a-z]
| Any single character in the range a-z

| [a-zA-Z]
| Any single character in the range a-z or A-Z

| (...)
| Capture everything enclosed

| (a|b)
| a or b

| a?
| Zero or one of a

| a*
| Zero or more of a

| a+
| One or more of a

| a{3}
| Exactly 3 of a

| a{3,}
| 3 or more of a

| a{3,6}
| Between 3 and 6 of a

| i
| case insensitive

| m
| make dot match newlines

| x
| ignore whitespace in regex

| o
| perform #{...} substitutions only once

|===

=== The dot

If you refer the table, the dot `.` in regexp is used to match anything. Take a regexp  `r".at"`, the `.at` means that it will match anything that's before `at`, so it matches `rat` in `There is rat in my house"`:

[source, julia]
----
match(r".at", "There is rat in my house")
----


Output:

----
RegexMatch("rat")
----

It matches `cat` in `"There is cat in my house"`:

[source, julia]
----
match(r".at", "There is cat in my house")
----


Output:

----
RegexMatch("cat")
----

It matches `bat` in `"There is bat in my house"`

[source, julia]
----
match(r".at", "There is bat in my house")
----


Output:

----
RegexMatch("bat")
----

=== Character classes

The dot before at `.at`, matches anything before `.at`, but let's say we want to match only, `b`, `c` and `r` before `at`, we can group them as character class like this `[bcr]`, where we put them inside square braces, now we follow it with at like this `[bcr]at`, this matches `bat`, `cat`, `rat`, but notthing else.

Take a look at the examples below where this regular expression `r"[bcr]at"` is put into action and matches stuff:

[source, julia]
----
match(r"[bcr]at", "There is rat in my house")
----


Output:

----
RegexMatch("rat")
----



[source, julia]
----
match(r"[bcr]at", "There is cat in my house")
----


Output:

----
RegexMatch("cat")
----



[source, julia]
----
match(r"[bcr]at", "There is bat in my house")
----


Output:

----
RegexMatch("bat")
----

But in the example below, we have a string that has no bat, cat or rat in it, so it returns nothing:

[source, julia]
----
match(r"[bcr]at", "There is mat in my house")
----

Below are simple programs where regexp `r"[bcr]at"` is used to detect presence of the animal names bat, cat or rat, if yes it prints `here is an animal in the house.` else it prints `I don't detect an animal.`.

[source, julia]
----
string = "There is rat in my house"

match_data = match(r"[bcr]at", string)

if match_data != nothing
    println("There is an animal in the house.")
else
    println("I don't detect an animal.")
end
----


Output:
----
There is an animal in the house.
----



[source, julia]
----
string = "There is mat in my house"

match_data = match(r"[bcr]at", string)

if match_data != nothing
    println("There is an animal in the house.")
else
    println("I don't detect an animal.")
end
----


Output:
----
I don't detect an animal.
----

We can provide something like ranges in character classes, say `[A-Z]` means match anything from capital `A` to capital `Z` (both inclusive), so in the example below `U` is the first match, so it get's detected:

[source, julia]
----
match(r"[A-Z]", "this string contains UPPERCASE letter")
----


Output:

----
RegexMatch("U")
----

There is no capital letter in `"this string does not contains uppercase letter"`, so the below example returns nothing as a match:

[source, julia]
----
match(r"[A-Z]", "this string does not contains uppercase letter")
----

In computers `A` to `Z` are ordered before `a` to `z`, so if we give a regular expression `r"[A-z]"` as show below:

[source, julia]
----
match(r"[A-z]", "There is a letter")
----


Output:

----
RegexMatch("T")
----

It detects any letter, so the first letter `T` gets matched and we get `RegexMatch("T")` as output. How ever this same regexp does not match numbers, so we get nothing as out put in the program below:


[source, julia]
----
match(r"[A-z]", "12345")
----

If we want to match numbers, we can use `\d` or in case of character classes we can use `[0-9]` as shown below to return a positive match:

[source, julia]
----
match(r"[0-9]", "12345")
----


Output:

----
RegexMatch("1")
----

In character classes `^` (carrot) signifies not, so when we say `[^0-9]` it means detect any thing that not 0, 9 and and any thing in between, so the string in example below `"12345"` is just numbers, so nothing gets detected.

[source, julia]
----
match(r"[^0-9]", "12345")
----

How ever the same regep detects `a` in the string `"12345 abc"` below because its not a number:

[source, julia]
----
match(r"[^0-9]", "12345 abc")
----


Output:

----
RegexMatch(" ")
----

=== Anchors

Let's say you want to match something at the start and end of a string, you can use a thing called anchors, once again refer to things to remember section for this. Anchor's `\A` and `^` are used to denote start of a string, hence `r"\AHello"` and `r"^Hello"` checks if Hello exists at the start of the string, if yes it matches as shown in the below examples:


[source, julia]
----
match(r"\AHello", "Hello world!")
----


Output:

----
RegexMatch("Hello")
----



[source, julia]
----
match(r"^Hello", "Hello world!")
----


Output:

----
RegexMatch("Hello")
----

In the below example we don't have a match because `Hello` is not at the start of `"Say Hello world!"`:

[source, julia]
----
match(r"\AHello", "Say Hello world!")
----

Similarly anchors `\Z` and `$` are used to match something that should be present at  the end of a string, so look at the exples below and explain it to yourself:


[source, julia]
----
match(r"world!\Z", "Hello world!")
----

Output:

----
RegexMatch("world!")
----



[source, julia]
----
match(r"world!$", "Hello world!")
----

Output:

----
RegexMatch("world!")
----

In the example below, `world!` is not present at the end of `"Hello world!, said the computer"` , so there is no match and it returns `nothing`.

[source, julia]
----
match(r"world!\Z", "Hello world!, said the computer")
----

=== Captures

Regexp is used to detect a pattern in a string, but what if we want to see what string it had matched? Welcome to captures. Captures are created by wrapping regexp in round braces as shown:

[source, julia]
----
match(r"(abc)", "This string contains abc in it")
----


Output:

----
RegexMatch("abc", 1="abc")
----

In the above example we have wrapped `abc` in regexp like this: r"(abc)", so apart from returning a positive match `RegexMatch("abc")`, it will also return a capture.

Look at the captures below, first we capture `abc` like this: `(abc)`, inside the capture we once again capture `bc` ike this `(a(bc))`, execute the code:

[source, julia]
----
match(r"(a(bc))", "This string contains abc in it")
----


Output:

----
RegexMatch("abc", 1="abc", 2="bc")
----

Regexp start from the outside, so first it captures `abc` in `"This string contains abc in it"` and then it captures `bc` inside the first capture. So you end up with two captures like this: `RegexMatch("abc", 1="abc", 2="bc")`.

Look at the example below, execute it:

[source, julia]
----
match(r"(a(b(c)))", "This string contains abc in it")
----


Output:

----
RegexMatch("abc", 1="abc", 2="bc", 3="c")
----

Let's see how it works. So we have a capture as shown: `(a(b(c)))`. Forget the inner braces, so the outer most braces looks like `(abc)`, so `abc` is captured and we get `RegexMatch("abc", 1="abc")`. Next come to the inner braces that captures `bc` `(a(bc))`, so that become the second capture and we get `RegexMatch("abc", 1="abc", 2="bc")`. Now the most inner most capture is just `c` as in ``(a(b(c)))`, so just `c` is in third capture and we get `RegexMatch("abc", 1="abc", 2="bc", 3="c")`.

==== Capturing phone number

Let's look at something that might be practical. Let's say that one would write his or her phone number in this format: +91 8428050777, where +91 is the country code and 8428050777 is the phone number. Now we need to write a regexp that would capture it.

Look at the piece of code below and let's execute it:

[source, julia]
----
match(r"((\d{2}) (\d{10}))", "+91 8428050777")
----

Output:

----
RegexMatch("91 8428050777", 1="91 8428050777", 2="91", 3="8428050777")
----

NOTE: You need to refer Things to remember section in Regexp

So we have got first capture as the entire phone number. This is accomplished by the regular expression `r"(\d{2} \d{10})"`, where `r"(\d{2})"` matches the `91` in `"+91 8428050777"`, next there is a space, s the regexp is now `r"(\d{2}) "` and then there is a ten digit number , so its now `r"(\d{2} \d{10})"`. this would give a  match and capture like this: `RegexMatch("91 8428050777", 1="91 8428050777")`

Next we need to capture the country code which is nothing but the first two digits in the regexp, so the regexp now becomes like this: `r"((\d{2}) \d{10})"`, notice the braces around `\d{2}`. So this would produce a match and capture like this `RegexMatch("91 8428050777", 1="91 8428050777", 2="91")`.

Similarly we apply a capture around `\d{10}` for local area number so we get a regexp as shown: `r"((\d{2}) (\d{10}))"`, this gives us our final match and capture as like this: `RegexMatch("91 8428050777", 1="91 8428050777", 2="91", 3="8428050777")`.

in the program below, we see how to use captures, execute it and we will see baout it.


[source, julia]
----
match_data = match(r"((\d{2}) (\d{10}))", "+91 8428050777")

println("Phone Number: ", match_data[1])
println("Country code: ", match_data[2])
println("Local number: ", match_data[3])
----


Output:
----
Phone Number: 91 8428050777
Country code: 91
Local number: 8428050777
----

So the above code we assign `match(r"((\d{2}) (\d{10}))", "+91 8428050777")` to a variable named `match_data` in this line:

```julia
match_data = match(r"((\d{2}) (\d{10}))", "+91 8428050777")
```

So `match_data` contains `RegexMatch("91 8428050777", 1="91 8428050777", 2="91", 3="8428050777")`. Which means `match_data[1]` will have `"91 8428050777"`, `match_data[2]` will have `91` and `match_data[3]` will have the capture `"8428050777"`. We print those in the following lines of code:

```julia
println("Phone Number: ", match_data[1])
println("Country code: ", match_data[2])
println("Local number: ", match_data[3])
```

=== Counts

NOTE: Refer Things to remember section to understand the code.

Looks at the piece of code below, the regex `r"\d"` matches exactly one digit that is `8` in `"8420050777"`.

[source, julia]
----
match(r"\d", "8420050777") # match just one digit
----


Output:

----
RegexMatch("8")
----

Now say we want to match more than one digit, we could use the `+` sign after `\d` to get regular expression like `r"\d+"`. So this identifies a (sub)string having one or more digit as shown below:


[source, julia]
----
match(r"\d+", "8420050777") # + means one or more
----


Output:

----
RegexMatch("8420050777")
----

So the whole number `8420050777` gets matched.


Now `*` in regex means zero or more and `\s` means space, so type the code below and execute it:


[source, julia]
----
match(r"\w*\s*\d+", "Karthik 8420050777") # * zero or more
----


Output:

----
RegexMatch("Karthik 8420050777")
----

You must read it as zero or more printable characters `r"\w*"`, followed by zero or more spaces `r"\w*\s*"`, followed by one or more numbers `r"\w*\s*\d+"`. So the above example matches "Karthik 8420050777".

You can remove the name, increase or decrease spaces, and it would still work. I have removed the name in the example below and the above regex works as shown:

```julia
match(r"\w*\s*\d+", "8420050777") # * zero or more
```

Output:

```
RegexMatch("8420050777")
```

in the next example , we will try to form regular expression where it needs to match something like `"Karthik: 8420050777"`. The example below fails to produce a math because there is no colon in the string.

[source, julia]
----
match(r"\w*: \d+", "Karthik 8420050777")
----

To fix it we use the zero or one thing, which is denoted by `?` as shown below:

[source, julia]
----
match(r"\w*:? \d+", "Karthik 8420050777") # ? means zero o one
----


Output:

----
RegexMatch("Karthik 8420050777")
----

So the regex should be read as zero or more words/printable characters `r"\w*"`, followed by zero or one colon `r"\w*:"`, followed by exactly one space `r"\w*:? "`, followed by one or more numbers.

This would even match when there is a colon in between name and space as shown below:

[source, julia]
----
match(r"\w*:? \d+", "Karthik: 8420050777")
----

Output:

----
RegexMatch("Karthik: 8420050777")
----

Now let's capture the name and phone number:

[source, julia]
----
match(r"(\w*):? (\d+)", "Karthik: 8420050777")
----


Output:

----
RegexMatch("Karthik: 8420050777", 1="Karthik", 2="8420050777")
----

To make the regex more robust, let's introduce zero or more spaces between the `:` and did it as shown below:

[source, julia]
----
match(r"(\w*):?\s*(\d+)", "Karthik: 8420050777")
----


Output:

----
RegexMatch("Karthik: 8420050777", 1="Karthik", 2="8420050777")
----

Note in the above example we have added `\s*` between `:?` and `\d+`.

Now let's test our almost bullet proof regex with lot of spaces between name and number:


[source, julia]
----
match(r"(\w*):?\s*(\d+)", "Karthik:     8420050777")
----


Output:

----
RegexMatch("Karthik:     8420050777", 1="Karthik", 2="8420050777")
----

Let's say t hat you want to match exact number of counts, say only 4 numbers, you could use something lke this:

[source, julia]
----
match(r"\d{4}", "Karthik:     8420050777")
----


Output:

----
RegexMatch("8420")
----

Since we seen usage of `{<number>}` in other sections in Regex, I am not going to go more into this.

=== String to regexp

May be one day you need to write a piece of code where you need to construct a  regular expression dynamically. For that you need to construct a string, and this needs to be used in a regexp.

So look at the code below and execute it:

[source, julia]
----
string = "\\d{4}"
regex = Regex(string)
----


Output:

----
r"\d{4}"
----

When wen print the above string using `println(string)` it prints `\d{4}` footnote:[One might need to learn about escape sequence in strings https://www.youtube.com/watch?v=dJ4PnBXkzXI], we pass `string` to a function called `Regex` like this: `Regex(string)` and it gets converted to a regular expression. We do capture it in a variable named `regex`.

Now let's use the regexp to amtch some numbers:

[source, julia]
----
match(regex, "43248")
----


Output:

----
RegexMatch("4324")
----

It works!

Below is a code where I have use `repr` function to convert regex into string, it was done for curiosity. May be it could serve you some purpose some day:

[source, julia]
----
regexp = r"\d+"
println(repr(regexp))
----

Output:

----
r"\d+"
----

=== Case sensitive & insensitive match

We can tell a regexp to do case sensitive or insensitive match, for example take the example below:


[source, julia]
----
match(r"(?i)uppercase", "This matches UPPERCASE")
----

Output:

----
RegexMatch("UPPERCASE")
----

`r"uppercase"`  should not match `UPPERCASE` but it can be made to match it by adding `(?i)` to the prior of `uppercase`, so it becomes like this `r"(?i)uppercase`. This would match `UPPERCASE` in `"This matches UPPERCASE"`.

Since it's a case insensitive match, it also matches `uppercase` as shown below:


[source, julia]
----
match(r"(?i)uppercase", "This matches uppercase")
----


Output:

----
RegexMatch("uppercase")
----

A very similar example is shown below, I think you should be able to explain it by now:

[source, julia]
----
match(r"(?i)UPPERCASE", "This matches uppercase")
----


Output:

----
RegexMatch("uppercase")
----

`(?i)` switches on caseinsensivivity, wheras `(?-i)` switches it off. So look at the example below:

[source, julia]
----
# Mixing case sensitivity
match(r"sensitive(?i)caseless(?-i)sensitive", "sensitiveCASELESSsensitive")
----


Output:

----
RegexMatch("sensitiveCASELESSsensitive")
----

`r"sensitive"` matches `sensitive`, then caseinsenivity gets switched on using `(?i)`, so `r"sensitive(?i)caseless"` matches `sensitiveCASELESS`, now we make it case sensitive using this switch `(?-i)`, and hence `r"sensitive(?i)caseless(?-i)sensitive"` matches the whole `sensitiveCASELESSsensitive`, and we get a match.

=== Scanning

You might be interested to scan a string for regexp and collect the words that match it, for that kind of operations I have written a function `scan_regexp` as shown below. Type the code and execute it, we will see how it works.


[source, julia]
----
string = "I have bat, cat and rat in my house"

function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)

    output = []

    for word in words
        if match(regexp, word) != nothing
            push!(output, word)
        end
    end

    output
end

scan_regexp(r".at", string)
----


Output:

----
3-element Vector{Any}:
 "bat"
 "cat"
 "rat"
----

First we start with an empty function like this:


```julia
function scan_regexp(regexp, string)
end
```

Let us receive  the regular expression in variable `regexp` and the string that should be scanned in variable `string`.

Now if you refer Things to remember section, `\W` in regexp means non alphabetic character's, so we create a regular expression to scan all non alphabets and replace them with spaces in the following line `string = replace(string, r"\W" => " ")`.

```julia
function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
end
```

Next we split the string into words using the this line `words = split(string)`, so the function now becomes like this:

```julia
function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)
end
```

Let's now have a array named `output` that will collect all `words` that match the `regexp`

```julia

function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)

    output = []
end
```

Now for each word in word we compare if it matches the regular expression:


```julia
function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)

    output = []

    for word in words
        if match(regexp, word) != nothing
        end
    end
end
```

If it matches we push that word into output using the following code: `push!(output, word)`, so the function now looks like this:

```julia
function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)

    output = []

    for word in words
        if match(regexp, word) != nothing
            push!(output, word)
        end
    end
end
```

Finally we return the `output`:

```julia
function scan_regexp(regexp, string)
    string = replace(string, r"\W" => " ")
    words = split(string)

    output = []

    for word in words
        if match(regexp, word) != nothing
            push!(output, word)
        end
    end

    output
end
```

=== Learn more about regex

Regular expression is huge topic, we have just scratched the surface to give an introduction to you, but you need to be good with it if you are doing text mining and other operations. So I strongly encourage the reader to do more research, seek and learn about it.

You may find this gist to be useful https://gist.github.com/dataPulverizer/23c8d992d351d7faf0ed1c1966605b10.

You can also refer these books

. Introducing regular expressions https://www.amazon.com/dp/B008K9OGDA
. Mastering regular expressions https://www.amazon.com/dp/B007I8S1X0

