=== Passing Arguments

Let's come back to the hotel example. While ordering Briyani, you can pass some options to the waiter. Say you say that you do not need onions as side dish but want more Brinjal, then depending on the options your side dish will be tailored. These passing values to a function is technically in programming is called arguments.

Type the program below and execute it:

```julia
function printline1(length)
    println('*' ^ length)
end

i = 1

while i <= 10
    printline1(i)
    i += 1
end
```

Output:

    *
    **
    ***
    ****
    *****
    ******
    *******
    ********
    *********
    **********

So in the above program in these lines:

```julia
while i <= 10
    printline1(i)
    i += 1
end
```

We call `printline1` like this `printline1(i)` with an argument. This `i` is passed to variable called `length` and is available inside the body of the `printline1` function. With the `length` we vary the number of stars in the line as shown in below code:

```
function printline1(length)
    println('*' ^ length)
end
```

The Jupyter notebook for this blog is available here https://gitlab.com/datascience-book/code/-/blob/master/functions.ipynb.
