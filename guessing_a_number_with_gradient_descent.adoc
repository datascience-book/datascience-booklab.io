=== Guessing Number With Gradient Descent

NOTE: Get the Jupyter notebook for this section here https://gitlab.com/datascience-book/code/-/blob/master/guessing_number_with_gradient_descent.ipynb

NOTE: Video lecture for this section could be found here https://youtu.be/NNU7HgSDR-Q

First let's guesses a number with Gradient Descent.

==== Intuition

Let's say we have a number 42, we need to guess it. So in the piece of code below, we have `number` that's 42, `guess` , that's an initial random guess we make and `α`, which we call as step size. When yo descend down a hill, you don't spring like frog, you take small steps, similarly, we will change our `guess` in small steps to reach `number`.

```julia
number = 42
α = 0.01
guess = rand(0:100)
```

Output:


    29


So in the case above, the computer has done a wild guess of 29, which is way off 42, now how to make the guess slightly more closer to 42. First we compute the error as shown below:

```julia
error = guess - number
```

Output:


    -13

Then we recompute guess as shown below:

```julia
guess = guess - α * error
```

Output:


    29.13

As you see, we don't do `guess= guess - error`, but we multply `error` with `α`. `α` is called step size or learning constant.

So we see that we have moved guess slightly more closer to 42 by following the above steps. Now let's take 1000 steps.


```julia
for i in 1:1000
    error = guess - number
    guess = guess - α * error
end

guess
```

Output:


    41.99944438604583

As you see, taking 1000 steps makes our guess very much closer to 42 which is our destiny. Even if we take 10,000 or 1,000,000 steps we won't over shoot 42, that's because, as we go near 42, `error` becomes almost zero thus change in guess keeps decreasing.

==== Guessing Number

Now we have got some intuition about guessing number with gradient descent, let's write some code to visualize it. The code below is same as we have seen in the previous section, but we have got variables `guesses` and `errors` which record our guesses and errors.


```julia
number = 42
α = 0.01
guess = rand(0:100)
guesses = []
errors = []

for i in 1:1000
    error = guess - number
    guess = guess - α * error
    push!(guesses, guess)
    push!(errors, error)
end
```

Now let's plot our `guesses` against our destiny `number`


```julia
using Plots

number_line = [number for i in 1:1000]

visual = plot!(1:1000, number_line, label="destiny")
plot!(visual, guesses, label="guesses")
```

Output:

image::plots/guessing_number_with_gradient_descent/output_7_0.svg[]

As you can see initially, the random `guess` which is around 47, creases steeply, but at the same time our `error` to descends steeply thus limiting the rate of change of `guess`. Slowly the `guess` approaches number and error since it almost is `0` ensures that the guess never over shoots 42.

*Exercise:* Change the value of `α` to a value say 10 and see what happens? could you explain why?.

*Exercise:* Change `number` and rerun the above program. Does the plotted graph differ? How and why?
