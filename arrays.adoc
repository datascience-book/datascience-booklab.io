== Arrays

NOTE: Video lecture for this section could be found here https://youtu.be/qCPIr-vD1Ls

Arrays are nothing but a collection of some stuff. Imagine a rack with, where each compartment is numbered/indexed 1, 2 ,3 ... and so on and you put an item in each compartment. At a later time you can retrieve the item using the index.

The code for this blog can be  fount here https://gitlab.com/datascience-book/code/-/blob/master/arrays.ipynb, let's get practical here now. Launch your Jupyter Lab and try these out:

First lets create an array as shown below:

```julia
array = [1, 2, 3, 4, 5]
```

Output:

    5-element Array{Int64,1}:
     1
     2
     3
     4
     5

As you can see we have `1, 2, 3, 4, 5`, within `[` and `]`, that's how Julia knows we have created an array, and we assign it to variable named `array`. So the variable `array` points to the start of `[1, 2, 3, 4, 5]`.

To determine how many elements are present in an array, we use the `length()` function as shown below

```julia
length(array)
```
Output:

    5

I don't think it should be a mystery to you that we have 5 elements in `array`.

Now let's try `size()`:

```julia
size(array)
```

Output:

    (5,)

so it returns something weird, and it looks like this `(5,)`, this is called a Tuple, a very similar data structure like Array, but it's immutable. That is we cannot change it. We will look at it in the later blogs.


Now let's get into some kind of statistics, what about totaling the values in an array, take a look at the example below:

```julia
total = 0

for element in array
    total += element
end

println(total)
```

Output:

    15

We use the `for` loop here, in this line `for element in array`, in each iteration, `element` get the value of one element of `array`. That is in the first iteration `element` becomes `1` in the second `2` and so on. Now when it  hits `total += 0`, in the first iteration total becomes `0 + 1`, that is `1`, in the second iteration `1 + 2` that is `3` and so on.... Finally we print `total` here `println(total)` and its `15`.

Now let's see what the average is. Its nothing but `total` divided by number of elements in the array as shown below:

```julia
avg = total / length(array)
```

Output:

    3.0

In previous blogs we have seen how to find maximum of 2 or three numbers, but look at the program below. You can pack how many number you want in the `array` and it will find the maximum:

```julia
max = array[1]

for element in array
    max = max > element ? max : element
end

println(max)
```

Output:

    5

First we have a variable `max` which s assigned to the first element of the `array` here `max = array[1]`, now we have these lines:

```julia
for element in array
    max = max > element ? max : element
end
```

In the above lines, each element of the `array` is assigned to `element` and here `max = max > element ? max : element`, `max` takes on the value of `element` if it's greater than `element` and finally we print out `max` here `println(max)`.

Now let's see how to access 3rd element of an array:

```julia
array[3]
```

Output:

    3


Now we access elements 2 to 4:

```julia
array[2:4]
```

Output:

    3-element Array{Int64,1}:
     2
     3
     4

See that in the above example we pass range `2:4` between the brackets.

We wrote a long program to find maximum of an array, but we can also do it like this:


```julia
maximum(array)
```

Output:

    5


Here is the code to find minimum:


```julia
minimum(array)
```

Output:

    1


We can get cumulative sum of an array as shown:

```julia
cumsum(array)
```

Output:

    5-element Array{Int64,1}:
      1
      3
      6
     10
     15


This is how to find total or sum of an array:

```julia
sum(array)
```

Output:

    15


Now let's multiply all elements of an array by `7`:

```julia
array * 7
```

Output:

    5-element Array{Int64,1}:
      7
     14
     21
     28
     35


Now let's add `1` to all elements of the array:

```julia
array.+ 1
```

Output:

    5-element Array{Int64,1}:
     2
     3
     4
     5
     6

Note that we use a dot in `array.+ 1`, the dot `.` is a broadcasting operator which is used for element wise operations. I find that we can omit `.` for multiplying and dividing array by scalar.

In the example below we divide array by scalar:

```julia
array / 4
```

Output:

    5-element Array{Float64,1}:
     0.25
     0.5
     0.75
     1.0
     1.25

In the below example we subtract `4` from each element of the array, notice that we use the dot `.` operator:


```julia
array.- 4
```

Output:

    5-element Array{Int64,1}:
     -3
     -2
     -1
      0
      1

There is a function called `typeof()` in Julia that tells the variable type. Let's see what data type `array` is:

```julia
typeof(array)
```

Output:

    Array{Int64,1}

So it says `Array`, that is collection of stuff in an ordered format, it's elements are integers `Int`, and each element occupies `64` bits of space. Why don't you check what is the type of `array / 4`?

Now let's see how to add a new element to an array:

```julia
push!(array, 21)
```

Output:

    6-element Array{Int64,1}:
      1
      2
      3
      4
      5
     21

In the above case we have added `21` to the array using `push!()` function. As a first argument to `push!()` we give `array` and as second argument we give the element to be added that is `21`.

Wen ever you use `push!()`, you see that array gets modified as shown below. The exclamation mark, also called as bang `!` in computer science indicates that the argument been passed to it gets modified. Let's check the value of `array` now:

```julia
array
```

Output:

    6-element Array{Int64,1}:
      1
      2
      3
      4
      5
     21

Now let's pop out the last element of the array:

```julia
pop!(array)
```

Output:

    21


We use function called `pop!()`, since it comes with a bang `!`, we can expect `array` is modified here as shown below:

```julia
array
```

Output:

    5-element Array{Int64,1}:
     1
     2
     3
     4
     5


We push `3` into the array:

```julia
push!(array, 3)
sort(array)
```

Output:

    6-element Array{Int64,1}:
     1
     2
     3
     3
     4
     5


We check it again as though we don't believe in `push!()`:

```julia
array
```

Output:

    6-element Array{Int64,1}:
     1
     2
     3
     4
     5
     3


We reverse the order of elements in array

```julia
reverse(array)
```

Output:

    6-element Array{Int64,1}:
     3
     5
     4
     3
     2
     1

We sort the array here:

```julia
sort!(array)
```

Output:

    6-element Array{Int64,1}:
     1
     2
     3
     3
     4
     5

Since we used a function with bang `!` above, we can expect the result to be stored in variable `array` as shown below:

```julia
array
```

Output:

    6-element Array{Int64,1}:
     1
     2
     3
     3
     4
     5


Now let's do some real math, lets say we are buying some items, their quantities and prices are given below:

```julia
mangoes = 2
rice_in_kg = 5
eggs = 12

mango_price = 50
rice_price = 30
egg_price = 7

quantities = [mangoes, rice_in_kg, eggs]
prices = [mango_price, rice_price, egg_price];
```

The above cell does not spit any output, look at the semicolon `;` at the end of the last line that prevents the cell from cluttering your notebook with its outputs.

Since it's Julia, I can compute the total prices as shown below:

```julia
total_array = quantities.* prices
```

Output:


    3-element Array{Int64,1}:
     100
     150
      84

As you can see, you can compute the total cost of each item using just the star `*` operator. That's a lot of savings for you from writing lot of iterative code (if you know to code in other languages).

Now let's compute the sun of the above `total_array`

```julia
grand_total = sum(total_array)
```

Output:

    334

Julia provides `sum()` function that computes the total of an array.

Now let's use Julia's built in linear algebra package, and since `quantities` and `prices` are nothing but vectors, we can compute the total by taking it's dot product as shown:

```julia
using LinearAlgebra
dot(quantities, prices)
```

Output:

    334

In the above example we have learned to use or import a package, to import it we use `using <package name>`. Thankfully Julia has `LinearAlgebra` package built into it, but it's not imported at the start by default, so we need to import it using `using LinearAlgebra`. It provides a convenient `dot()` function to calculate the dot product.

Now say we want to be more concise, we can use the dot operator `⋅` provided by `LinearAlgebra` package.

```julia
quantities ⋅ prices
```
Output:

    334

In case you are wondering how to type `⋅`, type `\cdot` and press `Tab`.
